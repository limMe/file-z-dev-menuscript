# 点击UITextField时隐藏键盘

理论上添加以下方法就ok了：

```objectivec
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(![self.passcell isExclusiveTouch] && ![self.phonecell isExclusiveTouch])
    {
        [self.view endEditing:YES];
        [self.passcell resignFirstResponder];
        [self.phonecell resignFirstResponder];
    }
}
```

但是我也没搞懂为什么有时候不起效。更彻底的是加GestureRecongnizer了：

```objectivec
UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
[self.view addGestureRecognizer:tap];
```

然后实现如下方法:

```objectivec
-(void)dismissKeyboard {
    [self.searchField resignFirstResponder];
}
```

此外，实现代理并设置 `textField.delegate = self`后，可以设置回车即收起键盘:

```objectivec
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchField resignFirstResponder];
    return true;
}
```
