# CocoaPods使用

### 环境搭建

对于pod的环境配置网上教程很多了。以下两篇不错:

* [Code4app教程](http://code4app.com/article/cocoapods-install-usage)
* [推酷教程](http://www.tuicool.com/articles/AFbQZ36)

### 初始化pod

随便新建一个工程，终端里cd进到工程的目录里，就是有 `.xcodeproj` 文件的地方。然后:

```
$ vim Podfile
```

完了之后输入要抓取的pod包，以TiUP客户端为例，输入i进入编辑模式:

```
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'

pod 'AFNetworking', '~> 2.0'
pod 'SVProgressHUD', :head
pod 'FMDB', '~>2.0'
pod 'AprilSDK'
```
完了之后使用 `:wq` 保存。接下来就是下载这些文件:

```
$ pod install
```
实际上这种方式会很慢，建议加一个参数
```
$ pod install --verbose --no-repo-update
```

