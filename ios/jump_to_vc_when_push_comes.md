# 点击推送时跳转页面

###准备工作

收到推送的处理方法。extra里包含了参数值。
```objectivec
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo != nil) {
        NSData *jsonData = [userInfo[@"extra"] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:NSJSONReadingMutableContainers
                                                              error:&err];
        if(err) {
            NSLog(@"extra参数错误：%@",err);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"zPresentPushNavController"
                                                            object:nil
         userInfo:dic];
    }
    else{
        NSLog(@"nil");
    }
}
```

注意到这里使用了 `NSNotificationCenter` 来处理信息流，上面这个方法发送了这个名字的消息。下面这个在 `TSCBaseViewController` 注册了一个这个消息的观察者。

```objectivec
[[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(showPushModally:)
                                             name:zPresentPushNavController
                                           object:nil];
```

这里会触发的方法是：

```objectivec
-(void)showPushModally:(NSNotification *)noti
{
    //验证登录
    TSCOAuthToken *token = [TSCLoginModel readTokenFromLocal];
    if (token) {
        self.pushExtra = noti.userInfo;
        [self performSegueWithIdentifier:@"ToPushMsgNavController" sender:self];
    }
}
```

##视图跳转

首先要在Storyboard里面创建一个NavgationController, 然后从当前的 `BaseViewController` 指一个 `Modal` 类型的Segue过去，起个名字，然后在 `BaseViewController` 中：

```objectivec
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ToPushMsgNavController"]) {
        TSCPushMsgNavController *pushVC = segue.destinationViewController;
        //传递参数
        pushVC.pushExtra = [self.pushExtra copy];
    }
}
```

跨Storyboard做跳转的话，应当使用：

```objectivec
UIStoryboard *listSB = [UIStoryboard storyboardWithName:@"List" bundle:[NSBundle mainBundle]];
TSCFormDetailViewController *formVC= [listSB instantiateViewControllerWithIdentifier:@"formDetailVC"];
[self pushViewController:formVC animated:YES];
```
