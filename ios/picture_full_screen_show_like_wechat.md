# 概述

[demo源代码](http://code4app.com/ios/%E5%9B%BE%E7%89%87%E6%94%BE%E5%A4%A7%E6%98%BE%E7%A4%BA/53e8294f933bf08f248b4d1c)

这份代码里面 `ImgScrollView` 和 `TapImageView` 是必须的，剩下的ViewController则应该自己写。

首先，应当在想要使用的图片放大的ViewController.m中引入：

```objectivec
#import "ImgScrollView.h"
#import "TapImageView.h"
```

然后，在interface中实现必要的接口：

```objectivec
@interface XXXViewController ()<TapImageViewDelegate,ImgScrollViewDelegate,UIScrollViewDelegate>
{
    //整个滑来滑去的界面
    UIScrollView *myScrollView;
    //标记我滑到了哪个界面
    NSInteger currentIndex;

    UIView *markView;
    UIView *scrollPanel;

    ImgScrollView *lastImgScrollView;
}
```

然后实现委托方法：

```objectivec

- (void) tappedWithObject:(id)sender{

    [self.view bringSubviewToFront:scrollPanel];
    scrollPanel.alpha = 1.0;

    TapImageView *tmpView = sender;
    currentIndex = tmpView.tag - 10;

    //转换后的rect
    CGRect convertRect = [[tmpView superview] convertRect:tmpView.frame toView:self.view];

    CGPoint contentOffset = myScrollView.contentOffset;
    contentOffset.x = currentIndex*320;
    myScrollView.contentOffset = contentOffset;

    //添加
    [self addSubImgView];

    ImgScrollView *tmpImgScrollView = [[ImgScrollView alloc] initWithFrame:(CGRect){contentOffset,myScrollView.bounds.size}];
    [tmpImgScrollView setContentWithFrame:convertRect];
    [tmpImgScrollView setImage:tmpView.image];
    [myScrollView addSubview:tmpImgScrollView];
    tmpImgScrollView.i_delegate = self;

    [self performSelector:@selector(setOriginFrame:) withObject:tmpImgScrollView afterDelay:0.1];

}

- (void) tapImageViewTappedWithObject:(id)sender{
    ImgScrollView *tmpImgView = sender;
    //

    [UIView animateWithDuration:0.5 animations:^{
        markView.alpha = 0;
        [tmpImgView rechangeInitRdct];
    } completion:^(BOOL finished) {
        scrollPanel.alpha = 0;
        tmpImgView.hidden = true;
    }];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.frame.size.width;
    currentIndex = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}
```

以及两个自定义的方法：

```objectivec
- (void) addSubImgView{
    for (UIView *tmpView in myScrollView.subviews)
    {
        [tmpView removeFromSuperview];
    }
}

- (void) setOriginFrame:(ImgScrollView *) sender
{
    [UIView animateWithDuration:0.4 animations:^{
        [sender setAnimationRect];
        markView.alpha = 1.0;
    }];
}
```

最后，在需要使用这个的时候，初始化一堆东西。
```objectivec
    scrollPanel = [[UIView alloc] initWithFrame:self.view.bounds];
    scrollPanel.backgroundColor = [UIColor clearColor];
    scrollPanel.alpha = 0;
    [self.view addSubview:scrollPanel];

    markView = [[UIView alloc] initWithFrame:scrollPanel.bounds];
    markView.backgroundColor = [UIColor blackColor];
    markView.alpha = 0.0;
    [scrollPanel addSubview:markView];

    myScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [scrollPanel addSubview:myScrollView];
    myScrollView.pagingEnabled = YES;
    myScrollView.delegate = self;
    CGSize contentSize = myScrollView.contentSize;
    contentSize.height = self.view.bounds.size.height;
    contentSize.width = 320 * 1;
    myScrollView.contentSize = contentSize;

    TapImageView *tmpView = [[TapImageView alloc] initWithFrame:CGRectMake(5+105, 10, 100, 100)];
    tmpView.t_delegate = self;
    tmpView.image = [UIImage imageNamed:[NSString stringWithFormat:@"1.jpg"]];
    tmpView.tag = 11;
    [self.view addSubview:tmpView];
```
