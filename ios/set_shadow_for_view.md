# Set Shadow for View

给一个视图设置阴影非常简单，只需要下面几行代码：

```obejectivec
    _imageView.layer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    _imageView.layer.shadowOffset = CGSizeMake(4,4);//shadowOffset阴影偏移,x向右偏移4，y向下偏移4，默认(0, -3),这个跟shadowRadius配合使用
    _imageView.layer.shadowOpacity = 0.8;//阴影透明度，默认0
    _imageView.layer.shadowRadius = 4;//阴影半径，默认3
```


