# 绑定tapGesture到UIImageView上

绑定tapGesture到UIImageView上，使得该图片可以被点击

```objectivec
- (void)viewDidLoad {
    [super viewDidLoad];
    //给UIImageView添加点击事件
    self.phoneBtn.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTapOnPhoneBtn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnPhoneBtn)];
    [self.phoneBtn addGestureRecognizer:singleTapOnPhoneBtn];

    self.idBtn.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTapOnIdBtn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnIdBtn)];
    [self.idBtn addGestureRecognizer:singleTapOnIdBtn];

    // Do any additional setup after loading the view.
}
```

然后给出点击后的方法：

```objectivec
-(void)tapOnPhoneBtn {
    [[TMLoginModel defaultLoginModel] setIsLogined:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)tapOnIdBtn {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"点左边一个" message:@"点击左边一个好不好" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    [av show];
}
```
