# 用Method Swizzle实现Hook

[参考文章](http://blog.csdn.net/yiyaaixuexi/article/details/9374411)。这里甚至给出了这个方法经常会出现的一些陷阱和问题，话说这一篇还是妹纸写的。

### 基本原理

在Objective-C中调用一个方法，其实是向一个对象发送消息，查找消息的唯一依据是selector的名字。利用Objective-C的动态特性，可以实现在运行时偷换selector对应的方法实现，达到给方法挂钩的目的。

每个类都有一个方法列表，存放着selector的名字和方法实现的映射关系。IMP有点类似函数指针，指向具体的Method实现。

更换selector的IMP就可以达到我们想要的目的。

### 添加新方法

以NSArray为例。加一个自己的lastObject代码。

```objectivec
- (id)myLastObject
{
    id ret = [self myLastObject];
    NSLog(@"**********  myLastObject *********** ");
    return ret;
}
```

这个看起来甚至像是无穷递归。不过这个方法是用来替换原来的方法的。[self myLastObject] 将会执行真的 [self lastObject] 。不过我还是有些不懂：**到底是在外面调用[obj myLastObject]会实际执行lastObject方法，还是在myLastObject内部调用[self myLastObject]会实际执行lastObject方法。或者两者都是？**

### 添加方法\调换IMP

```objectivec
Method ori_Method =  class_getInstanceMethod([NSArray class], @selector(lastObject));
Method my_Method = class_getInstanceMethod([NSArray class], @selector(myLastObject));
method_exchangeImplementations(ori_Method, my_Method);

NSArray *array = @[@"0",@"1",@"2",@"3"];
NSString *string = [array lastObject];
NSLog(@"TEST RESULT : %@",string);
```

奎哥的写法是如下。貌似添加方法容易失败，交换IMP则是下下策：
```objectivec
SEL originalSelector = @selector(viewWillAppear:);
SEL swizzledSelector = @selector(tracking_viewWillAppear:);

Method originalMethod = class_getInstanceMethod(class, originalSelector);
Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);

BOOL didAddMethod =
class_addMethod(class,
                originalSelector,
                method_getImplementation(swizzledMethod),
                method_getTypeEncoding(swizzledMethod));

if (didAddMethod) {
    class_replaceMethod(class,
                        swizzledSelector,
                        method_getImplementation(originalMethod),
                        method_getTypeEncoding(originalMethod));
} else {
    method_exchangeImplementations(originalMethod, swizzledMethod);
}
```
