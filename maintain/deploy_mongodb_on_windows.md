# 在Windows上配置MongoDB

参考：[博客园](http://www.cnblogs.com/lecaf/archive/2013/08/23/mongodb.html)

首先，到[MongoDB官网](http://www.mongodb.org/downloads)下载安装包。目前来说，MongoDB逐渐只支持64位操作系统了，如果要使用32位版本，可能必须点`Previous`，我找到的一个是`2.6.9`版本的。


然后解压, cmd cd到解压目录，然后 `mongod -dbpath "d:\data\db"`,成功后会显示监听端口，一般是27017.

不妨**把这个cmd窗口关闭**，此时MongoDB停止运行，这样是不好的。所以我们把它添加为服务。首先得新建一个log文件夹，里面加一个MongoDB.log文件。然后：

```
mongod --dbpath "d:\data\db" --logpath "d:\data\log\MongoDB.log" --install --serviceName "MongoDB"
```

然后启动这个服务：
```
NET START MongoDB
```

再然后输入mongo就可以看到连接上服务了。到此配置就基本完成。此外把bin目录添加到环境变量，会让操作更加简便。
